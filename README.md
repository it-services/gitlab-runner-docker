# GitLab Runner in a Docker Container

This creates a Gitlab Runner instance running inside a Docker container as per [Run GitLab Runner in a container](https://docs.gitlab.com/runner/install/docker.html).

## Getting started

Clone this repository in `/opt` (that's how the volumes are set up). If you want to use a different path, you need to edit the `docker-compose.yml` file to specify the correct location of the config volume.

```
cd /opt
git clone git@code.vt.edu:it-services/gitlab-runner-docker.git
cd gitlab-runner-docker
docker-compose up -d
```

This will create an instance of GitLab Runner running inside a container named `gitlab-runner`.

## Running commands

Commands can be run using `docker-compose run`. For example, to register a new runner, use the command -

```
docker-compose run --rm gitlab-runner register
```

## Updating

Updating is as simple as pulling down the latest image, removing the existing container and restarting the container with the updated image.

```
docker-compose pull
docker-compose stop && docker-compose rm
docker-compose up -d --force-recreate
```

## Logs
```
docker-compose logs -f
```
